<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PagesController extends Controller {

	public function about()
	{
		// $data ['people'] = [];
		$data['people'] = [
		'de s2 <3',
		'gu lima',
		'gu da mata'
		];

		$data['pageTitle'] = 'about me page';

		return view('pages.about', $data);
	}

	public function contact()
	{
		return view('pages.contact');
	}

}
