<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class to_do extends Model {

	protected $fillable = [
		'title',
		'body',
		'done'
	];

}
