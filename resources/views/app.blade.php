<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>
		@if(isset($pageTitle))
			{{ $pageTitle }}
		@else
			my page!
		@endif
	</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{ elixir('css/all.css') }}">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-rc.2/css/select2.min.css" />

</head>
<body>
	<div class="container">
		
		@include('flash::message')
	
		@yield('content')
	
	</div>

		<script src="//code.jquery.com/jquery.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-rc.2/js/select2.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<script>
			$('#flash-overlay-modal').modal();
			// $('div.alert').not('.alert-important').delay(3000).slideUp(400);
		</script>
		@yield('footer')
</body>
</html>