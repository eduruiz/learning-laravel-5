@extends('app')

	@section('content')
	
	<h1>hello, strange</h1>
	
	@if(count($people))
		<h3>people i really like!</h3>
		
		<ul>
			@foreach($people as $person)
				<li>{{ $person }}</li>
			@endforeach
		</ul>
	@else
		<h3>sorry, i don't like anybody :(</h3>
	@endif

	<p>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum quibusdam, dolorem est doloremque, suscipit explicabo ducimus mollitia nemo voluptatum numquam vero illo cumque nulla necessitatibus. In officiis earum animi sequi!
	</p>
	@stop
