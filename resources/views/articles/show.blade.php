@extends('app')
@section('content')
	<article>
		<h1>{{ $article->title }}</h1>
		<p>
			{{ $article->body }}
		</p>
		
		@unless($article->tags->isEmpty())
			<h5>tags</h5>

			@foreach($article->tags as $tag)
				<ul>
					<li>{{ $tag->name }}</li>
				</ul>
			@endforeach

		@endunless
	</article>
@stop
