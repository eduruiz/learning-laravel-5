@extends('app')
@section('content')
	<h1>articles</h1>
	<time>current time: {{ $now }}</time>
	<hr>
	@foreach($articles as $article)
		<article>

			<small><time>{{ $article->published_at->diffForHumans() }}</time></small>
			<h3><a href="{{ url('/articles', $article->id) }}">{{ $article->title }}</a></h3>
			<p>
				{{ $article->body }}
			</p>
		</article>
	@endforeach
@stop
